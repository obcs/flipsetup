

function Setup-SSHKey {
  $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes","Description."
  $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No","Description."
  $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)


  # $question = "Have you activated your engineering account at teach.engr.oregonstate.edu yet?"
  # $result = $host.ui.PromptForChoice($question, "", $options, 1)
  # switch ($result) {
  #   0{

  #   }1{
  #     Write-Host "Do that and wait a couple minute for your resources to be activated"
  #     Return 
  #   }
  # }

  $onid_name = Read-Host -Prompt "Enter your ONID name (without the @oregonstate.edu)"
  $onid_name = $onid_name.Trim()

  if([string]::IsNullOrEmpty($onid_name)){
    Write-Host -ForegroundColor "red" "Name is empty, run the script again"
    Return
  }

  # Makes sure the user passes in the correct name. If its wrong, will mess local config file up
  $title = "Are you sure your ONID name is '$onid_name'?"
  $result = $host.ui.PromptForChoice($title, "", $options, 1)
  switch ($result) {
    0{

    }1{
      Write-Host -ForegroundColor "yellow" "Run the script again"
      Return 
    }
  }

  # Force means it will do nothing if it already exists
  mkdir -Force ~/.ssh

  $key_name = "flip"
  $target_file = "$HOME/.ssh/$key_name"

  # -N "" skips passphrase
  # -f is target file

  ssh-keygen -f $target_file -C "$onid_name" -N '""'

  Write-Host -ForegroundColor "green" "Copying your public key to the server - this will prompt for your ONID password"

  # This will prompt the user for their ONID credentials
  Get-Content "$target_file.pub" | ssh $onid_name@access.engr.oregonstate.edu "umask 077; mkdir -p ~/.ssh; cat >> ~/.ssh/authorized_keys";

  # Adds 'flip' as an alias for this connection to config file
  # Allows you to type "ssh flip" and connect to it using the credentials above

@"

Host flip
    HostName access.engr.oregonstate.edu
    User $onid_name
    IdentityFile ~/.ssh/$key_name

"@ | Out-File -Encoding ascii -Append -FilePath $HOME/.ssh/config


  Write-Host "You can now type 'ssh flip' to connect to the server without a password/DUO prompt"

  Read-Host -Prompt "Press enter to connect"

  ssh flip
}

Setup-SSHKey
