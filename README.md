
This is a small script that will setup SSH keys for Windows on OSU's `flip` servers (`access.engr.oregonstate.edu`) in order to bypass the password and DUO prompt. After the script is run, you can use the command `ssh flip` to SSH into the flip servers without being prompted for a password.

`Windows Terminal`, which is pre-installed on new versions of Windows, is the recommended way to connect to the servers. This replaces the older Putty and MobaXTerm.

It will generate a key pair, upload the public key to `flip`, and add a couple lines to your local `ssh config` file which will allow you to type `ssh flip` to connect to the engineering servers.

In order to upload the key to the server, it will have one password/DUO prompt. This will be the last time you use your password and DUO to get into the servers!

Open PowerShell (Windows Terminal is recommended) and run the following command:
```powershell
iwr obcs.gitlab.io/flipsetup/setup.ps1 | iex
```

This one-liner downloads the script and executes it.

Alternatively, you open <https://obcs.gitlab.io/flipsetup/setup.ps1> in the browser, copy the content, and paste it into PowerShell.
